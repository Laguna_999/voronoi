﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace Voronoi
{
    public class TriSet
    {

        public List<Triangle> triangles;
        public TriSet()
        {
            triangles = new List<Triangle>();
            Point tl = new Point(new Vector2f(0, 0));
            Point tr = new Point(new Vector2f(800, 0));
            Point bl = new Point(new Vector2f(0, 600));
            Point br = new Point(new Vector2f(800, 600));

            AddTriangle(new Triangle(tl, tr, bl));
            AddTriangle(new Triangle(bl, br, tr));
        }

        public void AddTriangle(Triangle tri)
        {
            if (tri != null)
            {
                triangles.Add(tri);
            }
        }

        public int GetNumberOfTriangles()
        {
            return triangles.Count;
        }

        internal void AddPoints(PointSet points)
        {
            foreach (var p in points.points)
            {
                AddPoint(p);
            }
        }

        public void AddPoint(Point point)
        {

            Triangle tri = GetContainingTri(point);
            //Console("Adding Point " + point.Index + " in Triangle " + tri.Index);
            RefineTriangle(GetContainingTri(point), point);
        }

        private void RefineTriangle(Triangle tri, Point point)
        {
            //Console("START ------------------------------------------ NumTris " + GetNumberOfTriangles());
            //Console("Starting Refine Triangle tri " + tri.Index);
            triangles.Remove(tri);
            //Console("RemovingTriangle tri " + tri.Index + " between points " + tri.point0.Index + " " + tri.point1.Index + " " + tri.point2.Index);
            //Console("Adding new Point " + point);
            Triangle t0 = new Triangle(tri.point0, tri.point1, point);
            Triangle t1 = new Triangle(point, tri.point1, tri.point2);
            Triangle t2 = new Triangle(tri.point2, tri.point0, point);
            //Console("Therefore creating new Triangle " + t0);
            //Console("Therefore creating new Triangle " + t1);
            //Console("Therefore creating new Triangle " + t2);

            triangles.Add(t0);
            triangles.Add(t1);
            triangles.Add(t2);
            //Console("CHECK Umkreis for tri " + t0);
            CheckPerimeter(t0);
            //Console("CHECK Umkreis for tri " + t1);
            CheckPerimeter(t1);
            //Console("CHECK Umkreis for tri " + t2);
            CheckPerimeter(t2);
            //Console("FINE ------------------------------------------ NumTris " + GetNumberOfTriangles());
        }




        private void CheckPerimeter(Triangle tri)
        {
            Vector2f perimeterCenter = new Vector2f();

            Vector2f centroid = new Vector2f(0, 0);
            centroid += new Vector2f(tri.point0.X, tri.point0.Y);
            centroid += new Vector2f(tri.point1.X, tri.point1.Y);
            centroid += new Vector2f(tri.point2.X, tri.point2.Y);
            centroid /= 3.0f;

            float D = 2 * (tri.point0.X * (tri.point1.Y - tri.point2.Y) + tri.point1.X * (tri.point2.Y - tri.point0.Y) + tri.point2.X * (tri.point0.Y - tri.point1.Y));

            float axsqplusaysy = ((tri.point0.X * tri.point0.X) + (tri.point0.Y * tri.point0.Y));
            float bxsqplusbysy = ((tri.point1.X * tri.point1.X) + (tri.point1.Y * tri.point1.Y));
            float cxsqpluscysy = ((tri.point2.X * tri.point2.X) + (tri.point2.Y * tri.point2.Y));

            perimeterCenter.X = (axsqplusaysy * (tri.point1.Y - tri.point2.Y) + bxsqplusbysy * (tri.point2.Y - tri.point0.Y) + cxsqpluscysy * ((tri.point0.Y - tri.point1.Y))) / D;
            perimeterCenter.Y = (axsqplusaysy * (tri.point2.X - tri.point1.X) + bxsqplusbysy * (tri.point0.X - tri.point2.X) + cxsqpluscysy * ((tri.point1.X - tri.point0.X))) / D;

            Vector2f arbitraryDistance1 = perimeterCenter - new Vector2f(tri.point0.X, tri.point0.Y);
            float radiusSquared1 = (arbitraryDistance1.X * arbitraryDistance1.X + arbitraryDistance1.Y * arbitraryDistance1.Y);

            Vector2f arbitraryDistance2 = perimeterCenter - new Vector2f(tri.point1.X, tri.point1.Y);
            float radiusSquared2 = (arbitraryDistance2.X * arbitraryDistance2.X + arbitraryDistance2.Y * arbitraryDistance2.Y);
            //Console.WriteLine(radiusSquared2 + " " + radiusSquared2);

            List<Point> allpoints = GetAllPoints();
            List<Point> otherPointsWithinPerimeter = new List<Point>();
            foreach (var p in allpoints)
            {
                if (p.Index == tri.point0.Index || p.Index == tri.point1.Index || p.Index == tri.point2.Index)
                {   // do not look at same triangles;
                    continue;
                }
                Vector2f differenceVector = new Vector2f(p.X, p.Y) - perimeterCenter;
                float distanceSquared = differenceVector.X * differenceVector.X + differenceVector.Y * differenceVector.Y;



                if (distanceSquared < radiusSquared1)
                {
                    otherPointsWithinPerimeter.Add(p);
                }



            }
            if (otherPointsWithinPerimeter.Count != 0)
            {
                //Console("Problem Detected. There are otherPointsWithinPerimeter " + otherPointsWithinPerimeter.Count);

                foreach (var otherPoint in otherPointsWithinPerimeter)
                {
                    //Console.Write(otherPoint + " ");

                    Triangle tri2 = GetOtherTriangle(tri, otherPoint);
                    if (tri2 != null)
                    {
                        //Console("Flipping");
                        Flip(tri, tri2);
                        break;
                    }
                    else
                    {
                        //Console("No Triangle connecting them . It is not possible to flip tris");
                    }
                }

            }

        }

        private Triangle GetOtherTriangle(Triangle tri, Point point)
        {
            Triangle retVal = null;

            foreach (var t in triangles)
            {
                if (t.ContainsPointWithIndex(point.Index))
                {
                    if (t.CommonEdge(tri))
                    {
                        retVal = t;
                        break;
                    }
                }
            }
            if (retVal == null)
            {
                //throw new Exception("no other triangle could be found");
            }

            return retVal;
        }

        private List<Point> GetAllPoints()
        {
            List<Point> allpoints = new List<Point>();

            foreach (var t in triangles)
            {
                bool p0in = false;
                bool p1in = false;
                bool p2in = false;
                foreach (var p in allpoints)
                {
                    if (t.point0.Index == p.Index)
                    {
                        p0in = true;
                    }
                    if (t.point1.Index == p.Index)
                    {
                        p1in = true;
                    }
                    if (t.point2.Index == p.Index)
                    {
                        p2in = true;
                    }
                }
                if (!p0in)
                {
                    allpoints.Add(t.point0);
                }
                if (!p1in)
                {
                    allpoints.Add(t.point1);
                }
                if (!p2in)
                {
                    allpoints.Add(t.point2);
                }
            }

            return allpoints;
        }

        private List<Triangle> GetNeighbours(Triangle tri)
        {
            List<Triangle> neighbours = new List<Triangle>();

            neighbours.AddRange(GetTrisWithEdgePoint(tri.point0));
            neighbours.Remove(tri);

            neighbours.AddRange(GetTrisWithEdgePoint(tri.point1));
            neighbours.Remove(tri);

            neighbours.AddRange(GetTrisWithEdgePoint(tri.point2));
            neighbours.Remove(tri);

            return neighbours;
        }

        private List<Triangle> GetTrisWithEdgePoint(Point point)
        {
            List<Triangle> commonEdgeTris = new List<Triangle>();

            foreach (var t in triangles)
            {
                if (t.point0.Index == point.Index || t.point1.Index == point.Index || t.point2.Index == point.Index)
                {
                    commonEdgeTris.Add(t);
                }
            }
            return commonEdgeTris;
        }


        public void Flip(Triangle tri1, Triangle tri2)
        {
            if (!tri1.CommonEdge(tri2))
            {
                throw new Exception("Error, cannot flip triangles that do not have a common line");
            }

            List<Point> all = new List<Point>();

            all.Add(tri1.point0);
            all.Add(tri1.point1);
            all.Add(tri1.point2);

            all.Add(tri2.point0);
            all.Add(tri2.point1);
            all.Add(tri2.point2);

            Point single1 = null;
            Point single2 = null;
            Point double1 = null;
            Point double2 = null;

            for (int i = 0; i != all.Count; ++i)
            {
                if (tri1.ContainsPointWithIndex(all[i].Index) && tri2.ContainsPointWithIndex(all[i].Index))
                {
                    if (double1 == null)
                    {
                        double1 = all[i]; continue;
                    }
                    else
                    {
                        if (double1 != all[i])
                        {
                            double2 = all[i]; continue;
                        }
                    }

                }
                else
                {
                    if (single1 == null)
                    {
                        single1 = all[i]; continue;
                    }
                    else
                    {
                        single2 = all[i]; continue;
                    }
                }
            }

            triangles.Remove(tri1);
            triangles.Remove(tri2);


            Triangle flippedTri1 = new Triangle(single1, double1, single2);
            Triangle flippedTri2 = new Triangle(single1, double2, single2);

            AddTriangle(flippedTri1);
            AddTriangle(flippedTri2);
            CheckPerimeter(flippedTri1);
            CheckPerimeter(flippedTri2);
        }



        public Triangle GetContainingTri(Point point)
        {
            foreach (var t in triangles)
            {
                if (t.Contains(point))
                {
                    return t;
                }
            }
            throw new Exception("Point is not contained in any Triangle.");
        }

        public void Draw(RenderWindow rw)
        {
            foreach (var t in triangles)
            {
                t.Draw(rw);
            }
        }




    }
}

