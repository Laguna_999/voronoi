﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JamUtilities;
using SFML.Graphics;
using SFML.Window;

namespace Voronoi
{
    class Program
    {


        static void OnClose(object sender, EventArgs e)
        {
            // Close the window when OnClose event is received
            SFML.Graphics.RenderWindow window = (SFML.Graphics.RenderWindow)sender;
            window.Close();
        }

        static void Main(string[] args)
        {
            var applicationWindow = new RenderWindow(new VideoMode(803, 603, 32), "Delaunay");

            applicationWindow.SetFramerateLimit(60);
            applicationWindow.SetVerticalSyncEnabled(true);
            applicationWindow.Closed += new EventHandler(OnClose);
            int startTime = Environment.TickCount;
            int endTime = startTime;
            float time = 16.7f; // 60 fps -> 16.7 ms per frame

            PointSet points = new PointSet();
            TriSet triSet = new TriSet();
            //Console(triSet.GetNumberOfTriangles());

            points.AddPoint(new Point(new Vector2f(50, 300)));
            points.AddPoint(new Point(new Vector2f(750, 300)));
            points.AddPoint(new Point(new Vector2f(399, 550)));
            points.AddPoint(new Point(new Vector2f(450, 50)));
            points.AddRandomPoints(50);

            triSet.AddPoints(points);






            while (applicationWindow.IsOpen())
            {

                if (Keyboard.IsKeyPressed(Keyboard.Key.Escape))
                {
                    applicationWindow.Close();
                }

                if (startTime != endTime)
                {
                    time = (float)(endTime - startTime) / 1000.0f;
                }
                startTime = Environment.TickCount;

                if (SFML.Window.Mouse.IsButtonPressed(SFML.Window.Mouse.Button.Left))
                {

                    Vector2f mousePos = new Vector2f(SFML.Window.Mouse.GetPosition(applicationWindow).X, 600 - SFML.Window.Mouse.GetPosition(applicationWindow).Y);
                    if (mousePos.X > 0 && mousePos.X < 800 && mousePos.Y > 0 && mousePos.Y < 600)
                    {
                        //Console(triSet.GetContainingTri(new Point(mousePos, true)).ToString());
                    }
                }

                //if (points.points.Count == 1)
                //{
                //    if (SFML.Window.Keyboard.IsKeyPressed(Keyboard.Key.K))
                //    {
                //        points.AddPoint(new Point(new Vector2f(600, 100)));
                //        triSet.AddPoint(new Point(new Vector2f(600, 100)));
                //    }
                //}
                //if (points.points.Count == 0)
                //{
                //    if (SFML.Window.Keyboard.IsKeyPressed(Keyboard.Key.L))
                //    {
                //        points.AddPoint(new Point(new Vector2f(500, 500)));
                //        triSet.AddPoint(new Point(new Vector2f(500, 500)));
                //    }
                //}
                //if (points.points.Count == 2)
                //{
                //    if (SFML.Window.Keyboard.IsKeyPressed(Keyboard.Key.M))
                //    {
                //        //points.AddPoint(new Point(new Vector2f(500, 500)));
                //        //triSet.AddPoint(new Point(new Vector2f(500, 500)));
                //        //triSet.Flip()
                //    }
                //}


                applicationWindow.Clear(Color.White);

                triSet.Draw(applicationWindow);

                points.Draw(applicationWindow);

                applicationWindow.DispatchEvents();

                applicationWindow.Display();
                endTime = Environment.TickCount;
            }

        }
    }
}

