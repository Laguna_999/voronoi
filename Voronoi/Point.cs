﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace Voronoi
{
    public class Point
    {
        public Point(Vector2f p, bool temp = false)
        {
            X = p.X;
            Y = p.Y;
            if (!temp)
            {
                Index = myIndex;
                myIndex++;
            }
            else
            {
                Index = -1;
            }
        }

        public float X { get; set; }
        public float Y { get; set; }

        public int Index { get; private set; }

        private static int myIndex = 0;

        public override string ToString()
        {
            //return "(" + X + ", " + Y + ")";
            return Index.ToString();
        }
    }
}
