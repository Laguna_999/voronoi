﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JamUtilities;
using SFML.Graphics;
using SFML.Window;

namespace Voronoi
{
    public class PointSet
    {
        public List<Point> points;

        private CircleShape pointShape;

        public PointSet()
        {
            points = new List<Point>();
            pointShape = new CircleShape(6, 16);
            pointShape.FillColor = Color.Black;
        }

        public void AddPoint(Point p)
        {
            points.Add(p);
        }

        public void AddRandomPoints(uint number)
        {
            if (number != 0)
            {
                for (uint i = 0; i != number; i++)
                {
                    AddPoint(new Point(RandomGenerator.GetRandomVector2fInRect(new FloatRect(50, 50, 700, 500))));
                }
            }
        }

        public void Draw(RenderWindow rw)
        {
            if (points.Count != 0)
            {
                foreach (var vec in points)
                {
                    pointShape.Position = new Vector2f(vec.X - 6, 600 - (vec.Y + 6));
                    rw.Draw(pointShape);
                }
            }
        }
    }
}
