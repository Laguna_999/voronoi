﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JamUtilities;
using SFML.Graphics;
using SFML.Window;

namespace Voronoi
{
    public class Triangle
    {

        public Triangle(Point p0, Point p1, Point p2)
        {
            point0 = p0;
            point1 = p1;
            point2 = p2;

            CheckOrientation();
            LineCreator.CreateLine(out line0, new Vector2f(p0.X, 600 - p0.Y), new Vector2f(p1.X, 600 - p1.Y), 1);
            LineCreator.CreateLine(out line1, new Vector2f(p1.X, 600 - p1.Y), new Vector2f(p2.X, 600 - p2.Y), 1);
            LineCreator.CreateLine(out line2, new Vector2f(p2.X, 600 - p2.Y), new Vector2f(p0.X, 600 - p0.Y), 1);
            line0.FillColor = Color.Black;
            line1.FillColor = Color.Black;
            line2.FillColor = Color.Black;
            Index = maxIndex;
            maxIndex++;
        }

        private static int maxIndex = 0;

        public int Index { get; private set; }

        private void CheckOrientation()
        {
            // calculate cross product z component

            Vector2f d31 = new Vector2f(point0.X - point2.X, point0.Y - point2.Y);
            Vector2f d32 = new Vector2f(point1.X - point2.X, point1.Y - point2.Y);
            float x1 = d31.X;
            float y1 = d31.Y;
            float x2 = d32.X;
            float y2 = d32.Y;
            float crossProductZ = x1 * y2 - x2 * y1;

            // make sure the triangle is aligned correctly
            if (crossProductZ < 0)
            {
                Point t = point0;
                point0 = point1;
                point1 = t;
            }
        }


        RectangleShape line0;
        RectangleShape line1;
        RectangleShape line2;

        public Point point0;
        public Point point1;
        public Point point2;

        public bool Contains(Point vec)
        {
            return PointInTriangle(vec, point0, point1, point2);
        }


        // check if point p1 is left/right of line p2, p3
        private float sign(Point p1, Point p2, Point p3)
        {
            return (p1.X - p3.X) * (p2.Y - p3.Y) - (p2.X - p3.X) * (p1.Y - p3.Y);
        }

        private bool PointInTriangle(Point pt, Point v1, Point v2, Point v3)
        {
            bool b1, b2, b3;

            b1 = sign(pt, v1, v2) < 0.0f;
            b2 = sign(pt, v2, v3) < 0.0f;
            b3 = sign(pt, v3, v1) < 0.0f;

            return ((b1 == b2) && (b2 == b3));
        }

        public bool ContainsPointWithIndex(int i)
        {
            return (point0.Index == i || point1.Index == i || point2.Index == i);
        }

        public bool CommonEdge(Triangle tri)
        {
            if (tri.ContainsPointWithIndex(point0.Index) == false && tri.ContainsPointWithIndex(point1.Index) == false)
            {
                return false;
            }
            if (tri.ContainsPointWithIndex(point1.Index) == false && tri.ContainsPointWithIndex(point2.Index) == false)
            {
                return false;
            }
            if (tri.ContainsPointWithIndex(point2.Index) == false && tri.ContainsPointWithIndex(point0.Index) == false)
            {
                return false;
            }
            else
                return true;

        }

        public void Draw(RenderWindow rw)
        {
            rw.Draw(line0);
            rw.Draw(line1);
            rw.Draw(line2);
        }

        override public string ToString()
        {
            return Index.ToString() + " with points " + point0 + " " + point1 + " " + point2;
        }
    }
}
